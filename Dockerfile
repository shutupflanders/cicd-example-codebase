FROM php:7.4-cli

COPY app /app
EXPOSE 8080

WORKDIR /app
ENTRYPOINT ["php", "-S", "0.0.0.0:8080"]